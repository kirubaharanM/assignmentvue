import { mount } from "@vue/test-utils";
import expect from "expect";
import Example from '../src/js/components/ExampleComponent.vue';

describe('Example',  () => {
    it('displays example', () => {
       let wrapper = mount(Example);
       expect(wrapper.html()).toContain('Hi from Example component!');
    });
});
