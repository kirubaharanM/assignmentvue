import { shallowMount, mount} from "@vue/test-utils";
import expect from "expect";
import SortableList from '../src/js/components/SortableList.vue';

describe('SortableList', () => {
    let items = [{id: 1, title: 'FirstPost'},{id: 2, title: 'SecondPost'},{id: 3, title: 'ThirdPost'}];
    let wrapper;

    beforeEach(() => {
        wrapper = mount(SortableList, {
            propsData: {
                value: items
            }
        });
    })

   it('renders the given list', () => {
       expect(wrapper.findAll('.sortable-list-item').length).toEqual(3)
   });

    it('renders the first item', () => {
        let firstItem = wrapper.findAll('.sortable-list-item').at(0);
        expect(firstItem.text()).toContain('FirstPost');
    });

    it('does not add up arrow on first element', () => {
        let firstItem = wrapper.find('.sortable-list-item');
        expect(firstItem.findAll('.handler-move-up').length).toBe(0);
    })

    it('does not add down arrow on last element', () => {
        let items = wrapper.findAll('.sortable-list-item');
        expect(
            items.at(items.length-1).findAll('.handler-move-down').length
        ).toBe(0);
    })

    it('fires an event with changed items on up arrow click', () =>{
        let items = wrapper.findAll('.sortable-list-item');
        let lastItemHandler = items.at(items.length-1).find('.handler-move-up')
        lastItemHandler.trigger('click');

        expect(wrapper.emitted().sorted).toBeTruthy()
        expect(wrapper.emitted().sorted[0][1][1].title).toBe('ThirdPost')
    })

    it('fires an event with changed items on down arrow click', () =>{
        let handler = wrapper.find('.sortable-list-item .handler-move-down');
        handler.trigger('click');

        // let firstItem = wrapper.findAll('.sortable-list-item').at(0);
        expect(wrapper.emitted().sorted).toBeTruthy()
        expect(wrapper.emitted().sorted[0][1][0].title).toBe('SecondPost')
    })


});