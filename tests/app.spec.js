import { mount } from "@vue/test-utils";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import expect from "expect";
import App from '../src/js/App.vue';
import Vuex from 'vuex';
import posts from './../src/js/store/modules/posts';
import sinon from 'sinon';
import Vue from 'vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('App', () => {

    let getters;
    let actions;
    let store
    beforeEach(() => {
        getters = {
            getAvailablePosts: () => [{id: 1, title: 'FirstPost'},{id: 2, title: 'SecondPost'},{id: 3, title: 'ThirdPost'}],
        };

        actions = {
            fetchAll: sinon.stub(),
            updatePosts: sinon.stub(),
        }

        store = new Vuex.Store({
            modules: {
                posts: {
                    namespaced: true,
                    getters,
                    actions,
                    state: {},
                }
            }
        })
    })



   it('renders the posts', () => {
       let wrapper = mount(App, {
           store, localVue
       });
       expect(wrapper.text()).toContain('FirstPost');
       expect(wrapper.text()).toContain('SecondPost');
       expect(wrapper.text()).toContain('ThirdPost');
   });

    it('records performed actions', () => {
        let wrapper = mount(App, {
            store, localVue
        });

        //0:FirstPost, 1:SecondPost, 2:ThirdPost => 0:SecondPost, 1:FirstPost, 2:ThirdPost
        let handler = wrapper.find('.sortable-list-item .handler-move-down');
        handler.trigger('click');

        expect(wrapper.findAll('.action-item').length).toBe(1);

        let actionItem = wrapper.find('.action-item');
        expect(actionItem.text()).toContain(`Moved FirstPost  from index 0 to index 1`);

    })

    it('rollback the action and delete the action history when clicking Time Trave', () => {

        let wrapper = mount(App, {
            store, localVue
        });

        //0:FirstPost, 1:SecondPost, 2:ThirdPost => 0:SecondPost, 1:FirstPost, 2:ThirdPost
        let handler = wrapper.find('.sortable-list-item .handler-move-down');
        handler.trigger('click');

        expect(wrapper.vm.commitedActions.length).toBe(1);

        let timeTravel = wrapper.find('button.time-travel');
        timeTravel.trigger('click')
        expect(wrapper.vm.commitedActions.length).toBe(0);

    })

});