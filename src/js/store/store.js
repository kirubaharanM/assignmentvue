import Vue from 'vue';
import Vuex from 'vuex';
import posts from './modules/posts';
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production'

export const store = new Vuex.Store({
    state: {
    },
    getters: {
    },
    actions: {
    },
    mutations: {
    },
    modules: {
        posts
    },
    strict: debug,
})