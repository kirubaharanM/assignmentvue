import axios from 'axios';

const state = {
    _all: []
};

const getters = {
    getAvailablePosts: (state) => {
        return state._all;
    }
}

const actions = {
    fetchAll( context ) {
        return new Promise( async (resolve, reject) => {
            try {
                let response = await axios.get('https://jsonplaceholder.typicode.com/posts');
                context.commit('SET_POSTS', response.data.slice(0,5) );
                resolve(response)
            } catch (e) {
                reject(e);
            }
        })
    },
    updatePosts({commit}, updatedArray) {
        //Do any Ajax update, if necessory
        commit('SET_POSTS', updatedArray );
    }
}

const mutations = {
    SET_POSTS(state, posts) {
        state._all = posts
    }
}

export default {
    namespaced: true,
    state, getters, actions, mutations
}