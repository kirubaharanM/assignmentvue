import Vue from 'vue';
import exampleComponent from './components/ExampleComponent.vue';
import App from './app';
import { store } from './store/store'

Vue.component('app', App);

const main = new Vue({
    el: '#app',
    store,
    mounted: function() {
        console.log('Root Mounted');
    }
});